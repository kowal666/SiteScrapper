from urllib2 import Request, urlopen, HTTPError, URLError

def Url_checker():
    f = open("linki.txt","r")
    f1=open('links_after_check.txt',"w")
    for Link in f:
        user_agent = 'Mozilla/20.0.1 (compatible; MSIE 5.5; Windows NT)'
        headers = {'User-Agent': user_agent}
        req = Request(Link, headers=headers)
        try:
            page_open = urlopen(req)
        except HTTPError, e:
            print e.code
        except URLError, e:
            print e.reason
        else:
            print 'ok-'+str(Link)
            f1.write(Link)
    f1.close()
